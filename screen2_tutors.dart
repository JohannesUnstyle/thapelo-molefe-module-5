import 'package:flutter/material.dart';
import 'package:my_app/dashboard.dart';

class Tutors extends StatefulWidget {
  const Tutors({Key? key}) : super(key: key);

  @override
  State<Tutors> createState() => _TutorsState();
}

class _TutorsState extends State<Tutors> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Feature screen 2"),
      ),
      body: Column(
        children: [
          const Text(
            "Information search Tutors",
            style: TextStyle(
              color: Colors.black,
              fontSize: 30,
              fontWeight: FontWeight.w300,
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          const Text(
            "Online Marketing Tutors",
            style: TextStyle(
              color: Colors.black,
              fontSize: 30,
              fontWeight: FontWeight.w300,
            ),
          ),
          ElevatedButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const Dashboard(),
                ),
              );
            },
            child: const Text(
              "Back",
            ),
          )
        ],
      ),
    );
  }
}
