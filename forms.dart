import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class AddForm extends StatefulWidget {
  AddForm({Key? key}) : super(key: key);

  @override
  State<AddForm> createState() => _AddFormState();
}

class _AddFormState extends State<AddForm> {
  @override
  Widget build(BuildContext context) {
    TextEditingController subjectController = TextEditingController();
    TextEditingController locationController = TextEditingController();

    Future _addForm() {
      final subject = subjectController.text;
      final location = locationController.text;

      final ref = FirebaseFirestore.instance.collection("Notes").doc();
      return ref
          .set(
              {"Subject_Name": subject, "location": location, "doc_id": ref.id})
          .then((value) => ("Collection Message"))
          .catchError((onError) => (onError));
    }

    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: TextField(
              controller: subjectController,
              decoration: InputDecoration(
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: TextField(
              controller: locationController,
              decoration: InputDecoration(
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
              ),
            ),
            child: TextField(
              controller: locationController,
              decoration: InputDecoration(
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
              ),
            ),
          ),
          ElevatedButton(
              onPressed: () {
                _addForm();
              },
              child: const Text("Add notes"))
        ],
      ),
    );
  }
}
