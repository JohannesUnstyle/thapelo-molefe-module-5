import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:my_app/login.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: const FirebaseOptions(
          apiKey: "AIzaSyCpK1vlXtBVokP63dFRBPAVGm7rKfW3U9s",
          authDomain: "mymtnwebapp.firebaseapp.com",
          projectId: "mymtnwebapp",
          storageBucket: "mymtnwebapp.appspot.com",
          messagingSenderId: "830000201927",
          appId: "1:830000201927:web:de879145850ecffe933ee2"));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Home",
      home: AnimatedSplashScreen(
        duration: 3000,
        splash: Icons.home,
        backgroundColor: Colors.deepOrange,
        splashTransition: SplashTransition.rotationTransition,
        nextScreen: Login(),
      ),
      theme: ThemeData(
          primarySwatch: Colors.blue,
          scaffoldBackgroundColor: Colors.deepOrange),
    );
  }
}
